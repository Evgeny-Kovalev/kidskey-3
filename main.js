const app = new PIXI.Application({
	width: 800,
	height: 600,
	backgroundColor: 0x1099bb,
	resolution: window.devicePixelRatio || 1,
});
document.body.appendChild(app.view);

let square = 17320.508,
	angles = [60, 60, 60];

let lines = calcLinesEquation(square, angles);

if (lines) {
	for (let i = 0; i < 3; i++) {
		let myGraph1 = new PIXI.Graphics();
		app.stage.addChild(myGraph1);
		myGraph1.lineStyle(10, 0x000000)
			.moveTo(-1000, lines[i](-1000))
			.lineTo(1000, lines[i](1000));
		myGraph1.position.set(200, 200);
	}
}

function calcLinesEquation(S, angles) {
	let sum = angles.reduce((sum, elem) => {
		return sum + elem;
	}, 0);

	if (sum > 180 || sum < 0) return 0;

	let alpha = angles[0],
		beta = angles[1],
		gamma = angles[2],
		lines = [];

	let a = Math.round(Math.sqrt((2 * S * Math.sin(degToRad(alpha))) / (Math.sin(degToRad(beta)) * Math.sin(degToRad(gamma)))));
	let b = Math.round(Math.sqrt((2 * S * Math.sin(degToRad(beta))) / (Math.sin(degToRad(alpha)) * Math.sin(degToRad(gamma)))));
	let c = Math.round(Math.sqrt((2 * S * Math.sin(degToRad(gamma))) / (Math.sin(degToRad(alpha)) * Math.sin(degToRad(beta)))));

	let h = 2 * S / a;
	let halfA = Math.sqrt(c*c-h*h);

	lines[0] = (x) => {
		return 0;
	}

	lines[1] = (x) => {
		return calcY(x, {x1: a, y1: 0, x2: a-halfA, y2: h,});
	}

	lines[2] = (x) => {
		return calcY(x, {x1: 0, y1: 0, x2: a-halfA, y2: h,});
	}

	return lines;
}

function degToRad(deg) {
	return (Math.PI * deg) / 180;
}

function calcY(x, obj) {
	let y = (x-obj.x1)*(obj.y2-obj.y1)/(obj.x2-obj.x1)
	return y;
}